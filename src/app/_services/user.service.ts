import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`/users`);
    }
}

//
// import { Injectable } from '@angular/core';
// import { User } from '@int/.';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
// import { HttpClient } from '@angular/common/http';
// import { environment } from '@env/environment';
//
// @Injectable({
//   providedIn: 'root'
// })
// export class UserService {
//
//   env = environment;
//
//   userKey = 'gsAc185f31fc8de';
//
//   private _user$      = new BehaviorSubject<any>({});
//   public user$        = this._user$.asObservable();
//
//   constructor(private http: HttpClient) { }
//
//   // getUser() {
//   //   this.http.get<User>(this.env.apiBaseUrl + 'api/v1/common/users/me')
//   //     .subscribe(data => this._user$.next(data));
//   // }
//
//       getAll() {
//         this.http.get<User>(this.env.apiBaseUrl + 'api/v1/common/users/me')
//           .subscribe(data => this._user$.next(data));
//               }
//
//   removeUser() {
//     this._user$.next(false);
//   }
//
//   switchUser(dbid) {
//     return this.http.get<any>( this.env.apiBaseUrl + 'api/v1/client/' + dbid + '/family-users/switch/');
//   }
//
//   setLocalUser(data) {
//
//     localStorage.setItem(this.userKey, JSON.stringify(data));
//   }
//
//   removeLocalUser() {
//
//     localStorage.removeItem(this.userKey);
//   }
//
//   getLocalUser() {
//
//     let data = localStorage.getItem(this.userKey);
//
//     return ( data ) ? JSON.parse(data) : false;
//   }
// }
