// export interface Membership {
//   id: number,
//   label: string,
//   name: string
// }


export interface User {
  first_name: string,
  last_name: string,
  email: string,
  avatar: string
}

// membership: Membership,
// subusers: Array<Object>
